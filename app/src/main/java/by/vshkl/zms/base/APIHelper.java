package by.vshkl.zms.base;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public enum APIHelper {
    INSTANCE;

    public static ZoneminderAPI getZoneminderAPI(String base_url) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(ZoneminderAPI.BASE_URL_START + base_url + ZoneminderAPI.BASE_URL_END)
                .build()
                .create(ZoneminderAPI.class);
    }
}
