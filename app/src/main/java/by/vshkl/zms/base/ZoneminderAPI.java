package by.vshkl.zms.base;

import by.vshkl.zms.model.Events;
import by.vshkl.zms.model.Monitors;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface ZoneminderAPI {

    String BASE_URL_START = "http://";
    String BASE_URL_END = "/";

    @GET("zm/api/monitors.json")
    Observable<Monitors> getMonitors();

    @GET("zm/api/events/index/MonitorId:{monitorId}.json")
    Observable<Events> getEvents(
            @Path("monitorId") String monitorId,
            @Query("page") String pageNumber);

    @GET("zm/api/events/index/MonitorId:{monitorId}/StartTime%20%3E=:{startDate}%20{startTime}/EndTime%20%3C" +
            "=:{endDate}%20{endTime}.json")
    Observable<Events> getEventsByPeriod(
            @Path("monitorId") String monitorId,
            @Path("startDate") String startDate,
            @Path("endDate") String endDate,
            @Path("startTime") String startTime,
            @Path("endTime") String endTime,
            @Query("page") String pageNumber);

}
