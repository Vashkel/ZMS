package by.vshkl.zms.view;

import by.vshkl.zms.model.Events;

public interface IMonitorView {

    void onLoadSuccess(Events events);

    void onLoadingError(Throwable error);
}
