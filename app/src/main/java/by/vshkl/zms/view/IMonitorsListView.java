package by.vshkl.zms.view;

import by.vshkl.zms.model.IMonitors;

public interface IMonitorsListView {

    void onLoadSuccess(IMonitors monitors);

    void onLoadingError(Throwable error);

}
