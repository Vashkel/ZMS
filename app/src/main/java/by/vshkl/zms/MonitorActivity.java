package by.vshkl.zms;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.interfaces.OnChartValueSelectedListener;

import org.videolan.libvlc.IVLCVout;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import by.vshkl.zms.model.Events;
import by.vshkl.zms.presenter.IMonitorPresenter;
import by.vshkl.zms.presenter.MonitorPresenter;
import by.vshkl.zms.view.IMonitorView;

public class MonitorActivity extends AppCompatActivity
        implements IMonitorView, IVLCVout.Callback, View.OnClickListener, OnChartValueSelectedListener {

    private IMonitorPresenter mPresenter;
    private Events mEvents;

    private SurfaceHolder mHolder;
    @BindView(R.id.monitor_surface) SurfaceView mSurface;
    @BindView(R.id.monitor_play_pause) ImageButton mPlaybackButton;
    @BindView(R.id.card_view) CardView mCardView;
    @BindView(R.id.container) RelativeLayout mContainer;
    @BindView(R.id.container_barchart) FrameLayout mContainerBarchart;
    @BindView(R.id.events_barchart) BarChart mBarChart;
    @BindView(R.id.events_barchart_empty) TextView mEmptyView;

    private String mPath;
    private String mId;

    private LibVLC mLibVLC;
    private MediaPlayer mMediaPlayer = null;
    private Media mMedia;
    private final MediaPlayer.EventListener mPlayerListener = new MyPlayerListener(this);
    private int mVideoWidth;
    private int mVideoHeight;
    private boolean isPlaying = false;

    private final Handler mHandler = new Handler();

    /***********
     * Lifecycle
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitor);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        mPath = intent.getStringExtra("Path");
        mId = intent.getStringExtra("Id");

        mPlaybackButton.setOnClickListener(this);
        mPlaybackButton.setImageResource(R.drawable.ic_play);

        mBarChart.setOnChartValueSelectedListener(this);

        mSurface.setOnClickListener(this);

        mPresenter = new MonitorPresenter(this, this);
        mPresenter.loadEventsForTimePeriod(this, mId, "1");
    }

    @Override
    protected void onResume() {
        super.onResume();

        mHolder = mSurface.getHolder();

        createPlayer(mPath);
        if (isPlaying) {
            mMediaPlayer.play();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setSize(mVideoWidth, mVideoHeight);
            mContainerBarchart.setVisibility(View.GONE);
            mCardView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams
                    .MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            setSize(mVideoWidth, mVideoHeight);
            mContainerBarchart.setVisibility(View.VISIBLE);
            mCardView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams
                    .MATCH_PARENT, (int) (220 * this.getResources().getDisplayMetrics().density)));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        releasePlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releasePlayer();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.monitor_play_pause:
                togglePlayback(mHandler);
                break;
            case R.id.monitor_surface:
                togglePlaybackControls(mHandler);
                break;
        }
    }

    @Override
    public void onLoadSuccess(Events events) {
        mEvents = events;

        if (mEvents != null) {
            initChart();
        }
    }

    @Override
    public void onLoadingError(Throwable error) {
        error.printStackTrace();
        showSnackbar(getString(R.string.message_loading_failed));
    }


    @Override
    public void onValueSelected(Entry e, int dataSetIndex) {
        Toast toast = Toast.makeText(MonitorActivity.this,
                mEvents.getEvents().get(e.getXIndex()).getEvent().getName() + '\n'
                + mEvents.getEvents().get(e.getXIndex()).getEvent().getStartTime() + '\n'
                + mEvents.getEvents().get(e.getXIndex()).getEvent().getEndTime(),
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    @Override
    public void onNothingSelected() {

    }

    /************
     * Additional
     */

    private void togglePlaybackControls(Handler handler) {
        handler.removeCallbacksAndMessages(null);
        if (mPlaybackButton.getVisibility() == View.VISIBLE) {
            mPlaybackButton.setVisibility(View.GONE);
        } else {
            mPlaybackButton.setVisibility(View.VISIBLE);
            delayUiGone(handler);
        }
    }

    private void delayUiGone(Handler handler) {
        Runnable runnable = () -> mPlaybackButton.setVisibility(View.GONE);
        handler.postDelayed(runnable, 3000);
    }

    private void togglePlayback(Handler handler) {
        if (mMediaPlayer.isPlaying()) {
            stopPlayback();
            isPlaying = false;
        } else {
            startPlayback(handler);
            isPlaying = true;
        }
    }

    private void startPlayback(Handler handler) {
        mPlaybackButton.setImageResource(R.drawable.ic_pause);
        if (mMedia != null) {
            mMediaPlayer.play();
            delayUiGone(handler);
        }
    }

    private void stopPlayback() {
        mPlaybackButton.setImageResource(R.drawable.ic_play);
        mMediaPlayer.stop();
    }

    private void showSnackbar(String message) {
        Snackbar snackbar = Snackbar.make(mContainer, message, Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.action_retry, v -> {
            mPresenter.loadEvents(this, mId, "1");
        });
        snackbar.show();
    }

    private void initChart() {
        if (mEvents.getEvents().size() > 0) {
            mEmptyView.setVisibility(View.GONE);
            mBarChart.setVisibility(View.VISIBLE);

            ArrayList<BarEntry> entries = new ArrayList<>();
            ArrayList<String> labels = new ArrayList<>();

            int size = mEvents.getEvents().size();

            for (int i = 0; i < size; i++) {
                entries.add(new BarEntry(
                        Float.valueOf(mEvents.getEvents().get(i).getEvent().getMaxScore()),
                        i));
                labels.add(String.valueOf(i));
            }

            BarDataSet dataSet = new BarDataSet(entries, "Event max score");
            dataSet.setColors(new int[]{R.color.colorPrimary}, this);
            dataSet.setBarSpacePercent(75f);
            dataSet.setHighLightColor(ContextCompat.getColor(this, R.color.colorAccent));
            dataSet.setHighLightAlpha(255);

            BarData data = new BarData(labels, dataSet);

            mBarChart.setData(data);
            mBarChart.setDrawBarShadow(false);
            mBarChart.setDrawGridBackground(false);
            mBarChart.setDescription("Events timeline");
            mBarChart.setDrawXLabels(false);
            mBarChart.setDrawYLabels(false);
            mBarChart.setDrawHorizontalGrid(true);
            mBarChart.setDrawVerticalGrid(false);
            mBarChart.setScaleEnabled(false);
            mBarChart.setScaleMinima(data.getXValCount() / 20F, 1f);
            mBarChart.animateY(1000);
            mBarChart.invalidate();
        } else {
            mBarChart.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        }
    }

    /*************************
     * libVLC callback methods
     */

    @Override
    public void onNewLayout(IVLCVout vout, int width, int height,
                            int visibleWidth, int visibleHeight, int sarNum, int sarDen) {
        if (width * height == 0) {
            return;
        }

        mVideoWidth = width;
        mVideoHeight = height;
        setSize(mVideoWidth, mVideoHeight);
    }

    @Override
    public void onSurfacesCreated(IVLCVout ivlcVout) {

    }

    @Override
    public void onSurfacesDestroyed(IVLCVout ivlcVout) {

    }

    @Override
    public void onHardwareAccelerationError(IVLCVout ivlcVout) {
        Log.e("libVLC", "Error with hardware acceleration");
        this.releasePlayer();
    }

    /*********
     * Surface
     */

    private void setSize(int width, int height) {
        mVideoWidth = width;
        mVideoHeight = height;
        if (mVideoWidth * mVideoHeight <= 1) return;

        if (mHolder == null || mSurface == null) return;

        int screenWidth = getWindow().getDecorView().getWidth();
        int screenHeight = getWindow().getDecorView().getHeight();

        boolean mIsPortrait =
                getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        if (screenWidth > screenHeight && mIsPortrait ||
                screenWidth < screenHeight && !mIsPortrait) {
            int i = screenWidth;
            screenWidth = screenHeight;
            screenHeight = i;
        }

        float videoAspectRatio = (float) mVideoWidth / (float) mVideoHeight;
        float screenAspectRatio = (float) screenWidth / (float) screenHeight;

        if (screenAspectRatio < videoAspectRatio) {
            screenHeight = (int) (screenWidth / videoAspectRatio);
        } else {
            screenWidth = (int) (screenHeight * videoAspectRatio);
        }

        mHolder.setFixedSize(mVideoWidth, mVideoHeight);

        ViewGroup.LayoutParams layoutParams = mSurface.getLayoutParams();
        layoutParams.width = screenWidth;
        layoutParams.height = screenHeight;
        mSurface.setLayoutParams(layoutParams);
        mSurface.invalidate();
    }

    /********
     * Player
     */

    private void createPlayer(String mediaPath) {
        releasePlayer();
        try {
            ArrayList<String> options = new ArrayList<>();
            options.add("--aout=opensles");
            options.add("--audio-time-stretch");
            options.add("-vvv");
            mLibVLC = new LibVLC(options);
            mHolder.setKeepScreenOn(true);

            mMediaPlayer = new MediaPlayer(mLibVLC);
            mMediaPlayer.setEventListener(mPlayerListener);

            final IVLCVout videoOutput = mMediaPlayer.getVLCVout();
            videoOutput.setVideoView(mSurface);
            videoOutput.addCallback(this);
            videoOutput.attachViews();

            Uri uri = Uri.parse(mediaPath);
            mMedia = new Media(mLibVLC, uri);
            mMediaPlayer.setMedia(mMedia);
        } catch (Exception e) {
            Toast.makeText(this, "Error creating player!", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private void releasePlayer() {
        if (mLibVLC == null) {
            return;
        }
        mMediaPlayer.stop();
        final IVLCVout videoOutput = mMediaPlayer.getVLCVout();
        videoOutput.removeCallback(this);
        videoOutput.detachViews();
        mHolder = null;
        mLibVLC.release();
        mLibVLC = null;
        mVideoWidth = 0;
        mVideoHeight = 0;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*****************
     * Events listener
     */

    private static class MyPlayerListener implements MediaPlayer.EventListener {
        private final WeakReference<MonitorActivity> mOwner;

        public MyPlayerListener(MonitorActivity owner) {
            mOwner = new WeakReference<>(owner);
        }

        @Override
        public void onEvent(MediaPlayer.Event event) {
            MonitorActivity player = mOwner.get();

            switch (event.type) {
                case MediaPlayer.Event.EndReached:
                    Log.d("libVLC", "MediaPlayerEndReached");
                    player.releasePlayer();
                    break;
                case MediaPlayer.Event.Playing:
                case MediaPlayer.Event.Paused:
                case MediaPlayer.Event.Stopped:
                default:
                    break;
            }
        }
    }
}
