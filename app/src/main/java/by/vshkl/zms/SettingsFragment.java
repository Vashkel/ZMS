package by.vshkl.zms;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.PreferenceFragment;

public class SettingsFragment extends PreferenceFragment
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public void onResume() {
        super.onResume();

        getPreferenceScreen()
                .getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);

        updatePreferencesSummary();
    }

    @Override
    public void onPause() {
        super.onPause();

        getPreferenceScreen()
                .getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        updatePreferencesSummary();
    }

    private void updatePreferencesSummary() {
        EditTextPreference preferenceHost =
                (EditTextPreference) findPreference(getString(R.string.pref_host_key));
        EditTextPreference preferencePort =
                (EditTextPreference) findPreference(getString(R.string.pref_port_key));

        preferenceHost.setSummary(preferenceHost.getText());
        preferencePort.setSummary(preferencePort.getText());
    }
}
