package by.vshkl.zms.presenter;

import android.content.Context;

import by.vshkl.zms.model.IMonitor_;
import by.vshkl.zms.model.IMonitors;

public interface IMonitorsListPresenter {

    void loadMonitors(Context context);

    void saveMonitors(Context context, IMonitors monitors);

    void updateMonitors(Context context);

    void startNewActivity(Context context, IMonitor_ monitor);

    void searchMonitor(String searchQuery, Context context);
}
