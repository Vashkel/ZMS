package by.vshkl.zms.presenter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.cesarferreira.rxpaper.RxPaper;

import java.util.ArrayList;
import java.util.List;

import by.vshkl.zms.MonitorActivity;
import by.vshkl.zms.R;
import by.vshkl.zms.base.APIHelper;
import by.vshkl.zms.model.IMonitor_;
import by.vshkl.zms.model.IMonitors;
import by.vshkl.zms.model.Monitor;
import by.vshkl.zms.model.Monitors;
import by.vshkl.zms.view.IMonitorsListView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MonitorsListPresenter implements IMonitorsListPresenter {

    private final IMonitorsListView mView;
    private IMonitors mMonitors;
    private Throwable mError;

    public MonitorsListPresenter(IMonitorsListView view) {
        mView = view;
    }

    @Override
    public void loadMonitors(Context context) {
        RxPaper.with(context)
                .read("Monitors")
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        monitor -> {
                            mMonitors = (IMonitors) monitor;
                            mView.onLoadSuccess(mMonitors);
                        },
                        throwable -> {
                            mError = throwable;
                            updateMonitors(context);
                        }
                );
    }

    @Override
    public void saveMonitors(Context context, IMonitors monitors) {
        RxPaper.with(context)
                .write("Monitors", monitors)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        success -> {
                            Log.i("RxPaper", "RxPaper success on save.");
                        },
                        throwable -> {
                            Log.e("RxPaper", "RxPaper error on save.");
                        }
                );
    }

    @Override
    public void updateMonitors(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String host = preferences.getString(
                context.getString(R.string.pref_host_key),
                context.getString(R.string.pref_serv_host_def)
        );
        String port = preferences.getString(
                context.getString(R.string.pref_port_key),
                context.getString(R.string.pref_serv_port_def)
        );

        APIHelper.getZoneminderAPI(host + ":" + port)
                .getMonitors()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        success -> {
                            mMonitors = success;
                            mView.onLoadSuccess(mMonitors);
                        },
                        throwable -> {
                            mError = throwable;
                            mView.onLoadingError(mError);
                        }
                );
    }

    @Override
    public void startNewActivity(Context context, IMonitor_ monitor) {
        StringBuilder sbPath = new StringBuilder();
        if (monitor.getHost().equals("")) {
            sbPath.append(monitor.getPath());
        } else {
            sbPath.append(monitor.getProtocol())
                    .append("://")
                    .append(monitor.getHost())
                    .append(":")
                    .append(monitor.getPort())
                    .append(monitor.getPath());
        }

        Intent intent = new Intent(context, MonitorActivity.class);
        intent.putExtra("Path", sbPath.toString());
        intent.putExtra("Id", monitor.getId());

        context.startActivity(intent);
    }

    @Override
    public void searchMonitor(String searchQuery, Context context) {
        Monitors resultMonitors = new Monitors();
        List<Monitor> resultMonitorsList = new ArrayList<>();

        RxPaper.with(context)
                .read("Monitors")
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        monitor -> {
                            mMonitors = (IMonitors) monitor;
                        },
                        throwable -> {
                            mError = throwable;
                        }
                );

        for (Monitor monitor : mMonitors.getMonitors()) {
            if (monitor.getMonitor().getName().toLowerCase().contains(searchQuery)) {
                resultMonitorsList.add(monitor);
            }
        }

        resultMonitors.setMonitors(resultMonitorsList);

        mView.onLoadSuccess(resultMonitors);
    }
}
