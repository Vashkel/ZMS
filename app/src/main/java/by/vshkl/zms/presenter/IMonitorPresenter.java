package by.vshkl.zms.presenter;

import android.content.Context;

public interface IMonitorPresenter {

    void loadEvents(Context context, String monitorId, String pageNumber);

    void loadEventsForTimePeriod(Context context, String monitorId, String pageNumber);
}
