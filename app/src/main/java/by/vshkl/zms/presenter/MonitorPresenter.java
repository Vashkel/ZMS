package by.vshkl.zms.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import by.vshkl.zms.R;
import by.vshkl.zms.base.APIHelper;
import by.vshkl.zms.model.Events;
import by.vshkl.zms.view.IMonitorView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MonitorPresenter implements IMonitorPresenter {
    private static final int OFFSET = 7;
    private final IMonitorView mView;
    private Events mEvents;
    private Throwable mError;
    private String mHost;
    private String mPort;
    private String mTimeline;

    public MonitorPresenter(IMonitorView view, Context context) {
        mView = view;

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        mHost = preferences.getString(
                context.getString(R.string.pref_host_key),
                context.getString(R.string.pref_serv_host_def)
        );
        mPort = preferences.getString(
                context.getString(R.string.pref_port_key),
                context.getString(R.string.pref_serv_port_def)
        );
        mTimeline = preferences.getString(
                context.getString(R.string.pref_timeline_key),
                context.getString(R.string.pref_timeline_def)
        );
    }

    @Override
    public void loadEvents(Context context, String monitorId, String pageNumber) {
        APIHelper.getZoneminderAPI(mHost + ":" + mPort)
                .getEvents(monitorId, pageNumber)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        success -> {
                            if (mEvents != null) {
                                mEvents.addEvents(success.getEvents());
                            } else {
                                mEvents = success;
                            }
                            mView.onLoadSuccess(mEvents);
                        },
                        throwable -> {
                            mError = throwable;
                            mView.onLoadingError(mError);
                        }
                );
    }

    @Override
    public void loadEventsForTimePeriod(Context context, String monitorId, String pageNumber) {
        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");

        Date date = calendar.getTime();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, -7);
        String endDate = dateFormatter.format(calendar.getTime());
        String endTime = timeFormatter.format(calendar.getTime());

        date = calendar.getTime();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, -Integer.parseInt(mTimeline) -7);
        String startDate = dateFormatter.format(calendar.getTime());
        String startTime = timeFormatter.format(calendar.getTime());

        APIHelper.getZoneminderAPI(mHost + ":" + mPort)
                .getEventsByPeriod(monitorId, startDate, endDate, startTime, endTime, pageNumber)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        success -> {
                            mEvents = success;
                            mView.onLoadSuccess(mEvents);
                        },
                        throwable -> {
                            mError = throwable;
                            mView.onLoadingError(mError);
                        }
                );
    }
}
