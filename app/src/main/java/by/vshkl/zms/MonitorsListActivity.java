package by.vshkl.zms;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arlib.floatingsearchview.FloatingSearchView;

import butterknife.BindView;
import butterknife.ButterKnife;
import by.vshkl.zms.model.IMonitors;
import by.vshkl.zms.presenter.IMonitorsListPresenter;
import by.vshkl.zms.presenter.MonitorsListPresenter;
import by.vshkl.zms.utils.DividerItemDecoration;
import by.vshkl.zms.view.IMonitorsListView;

public class MonitorsListActivity extends AppCompatActivity implements IMonitorsListView,
        SwipeRefreshLayout.OnRefreshListener, FloatingSearchView.OnQueryChangeListener,
        FloatingSearchView.OnFocusChangeListener, FloatingSearchView.OnMenuItemClickListener {

    private IMonitorsListPresenter mPresenter;
    private IMonitors mMonitors;
    private Context mContext;

    @BindView(R.id.monitors_list_recycler_view) RecyclerView mMonitorsRecyclerView;
    @BindView(R.id.monitors_list_swipe_container) SwipeRefreshLayout mMonitorsSwipeRefresh;
    @BindView(R.id.container) RelativeLayout mContainer;
    @BindView(R.id.floating_search_view) FloatingSearchView mSearchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitors_list);
        ButterKnife.bind(this);

        mMonitorsSwipeRefresh.setOnRefreshListener(this);
        mSearchView.setOnQueryChangeListener(this);
        mSearchView.setOnFocusChangeListener(this);
        mSearchView.setOnMenuItemClickListener(this);

        mPresenter = new MonitorsListPresenter(this);
        mPresenter.loadMonitors(this);
        mContext = this;

        initRecyclerView();
    }

    @Override
    public void onLoadSuccess(IMonitors monitors) {
        mMonitors = monitors;
        mMonitorsRecyclerView.setAdapter(new MonitorsListAdapter());

        stopProgressIndicator();

        if (!mSearchView.isSearchBarFocused()) {
            mPresenter.saveMonitors(this, mMonitors);
        }
    }

    @Override
    public void onLoadingError(Throwable error) {
        error.printStackTrace();
        stopProgressIndicator();
        showSnackbar(getString(R.string.message_loading_failed));
    }

    @Override
    public void onRefresh() {
        mPresenter.updateMonitors(this);
    }

    @Override
    public void onSearchTextChanged(String oldQuery, String newQuery) {
        mPresenter.searchMonitor(newQuery, this);
    }

    private void initRecyclerView() {
        mMonitorsRecyclerView.setHasFixedSize(false);
        mMonitorsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mMonitorsRecyclerView.addItemDecoration(new DividerItemDecoration(this, null));
    }

    private void stopProgressIndicator() {
        mMonitorsSwipeRefresh.setRefreshing(false);
    }

    private void showSnackbar(String message) {
        Snackbar snackbar = Snackbar.make(mContainer, message, Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.action_retry, v -> {
            mPresenter.updateMonitors(this);
        });
        snackbar.show();
    }

    @Override
    public void onFocus() {
    }

    @Override
    public void onFocusCleared() {
        mPresenter.loadMonitors(this);
    }

    @Override
    public void onActionMenuItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
        }
    }

    /**********************
     * MonitorsList adapter
     */
    private class MonitorsListAdapter extends RecyclerView.Adapter<MonitorsListViewHolder> {

        @Override
        public MonitorsListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.item_monitor, parent, false);

            return new MonitorsListViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MonitorsListViewHolder holder, int position) {
            if (mMonitors != null) {
                holder.mMonitorId
                        .setText(mMonitors.getMonitors().get(position).getMonitor().getId());
                holder.mMonitorName
                        .setText(mMonitors.getMonitors().get(position).getMonitor().getName());
                holder.mMonitorMethod
                        .setText(mMonitors.getMonitors().get(position).getMonitor().getFunction());
                int state = Integer
                        .parseInt(mMonitors.getMonitors().get(position).getMonitor().getEnabled());
                if (state == 1) {
                    holder.mMonitorEventsValues.setText(R.string.monitor_state_enabled);
                } else {
                    holder.mMonitorEventsValues.setText(R.string.monitor_state_disabled);
                }
            }
        }

        @Override
        public int getItemCount() {
            if (mMonitors != null) {
                return mMonitors.getMonitors().size();
            } else {
                return 0;
            }
        }
    }

    /*************************
     * Monitor item ViewHolder
     */
    class MonitorsListViewHolder extends RecyclerView.ViewHolder implements View
            .OnClickListener {

        @BindView(R.id.monitor_id) TextView mMonitorId;
        @BindView(R.id.monitor_name) TextView mMonitorName;
        @BindView(R.id.monitor_method) TextView mMonitorMethod;
        @BindView(R.id.monitor_state) TextView mMonitorEventsValues;

        public MonitorsListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mPresenter.startNewActivity(mContext,
                    mMonitors.getMonitors().get(getAdapterPosition()).getMonitor());
        }
    }
}


