
package by.vshkl.zms.model;

public class Monitor_ implements IMonitor_ {

    private String Id;
    private String Name;
    private String ServerId;
    private String Type;
    private String Function;
    private String Enabled;
    private String LinkedMonitors;
    private String Triggers;
    private String Device;
    private String Channel;
    private String Format;
    private boolean V4LMultiBuffer;
    private String V4LCapturesPerFrame;
    private String Protocol;
    private String Method;
    private String Host;
    private String Port;
    private String SubPath;
    private String Path;
    private String Options;
    private String User;
    private String Pass;
    private String Width;
    private String Height;
    private String Colours;
    private String Palette;
    private String Orientation;
    private String Deinterlacing;
    private boolean RTSPDescribe;
    private String Brightness;
    private String Contrast;
    private String Hue;
    private String Colour;
    private String EventPrefix;
    private String LabelFormat;
    private String LabelX;
    private String LabelY;
    private String LabelSize;
    private String ImageBufferCount;
    private String WarmupCount;
    private String PreEventCount;
    private String PostEventCount;
    private String StreamReplayBuffer;
    private String AlarmFrameCount;
    private String SectionLength;
    private String FrameSkip;
    private String MotionFrameSkip;
    private String AnalysisFPS;
    private String AnalysisUpdateDelay;
    private String MaxFPS;
    private String AlarmMaxFPS;
    private String FPSReportInterval;
    private String RefBlendPerc;
    private String AlarmRefBlendPerc;
    private String Controllable;
    private String ControlId;
    private Object ControlDevice;
    private Object ControlAddress;
    private Object AutoStopTimeout;
    private String TrackMotion;
    private String TrackDelay;
    private String ReturnLocation;
    private String ReturnDelay;
    private String DefaultView;
    private String DefaultRate;
    private String DefaultScale;
    private String SignalCheckColour;
    private String WebColour;
    private boolean Exif;
    private String Sequence;

    public Monitor_() {
    }

    /**
     * @return The Id
     */
    public String getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     * @return The Name
     */
    public String getName() {
        return Name;
    }

    /**
     * @param Name The Name
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * @return The ServerId
     */
    public String getServerId() {
        return ServerId;
    }

    /**
     * @param ServerId The ServerId
     */
    public void setServerId(String ServerId) {
        this.ServerId = ServerId;
    }

    /**
     * @return The Type
     */
    public String getType() {
        return Type;
    }

    /**
     * @param Type The Type
     */
    public void setType(String Type) {
        this.Type = Type;
    }

    /**
     * @return The Function
     */
    public String getFunction() {
        return Function;
    }

    /**
     * @param Function The Function
     */
    public void setFunction(String Function) {
        this.Function = Function;
    }

    /**
     * @return The Enabled
     */
    public String getEnabled() {
        return Enabled;
    }

    /**
     * @param Enabled The Enabled
     */
    public void setEnabled(String Enabled) {
        this.Enabled = Enabled;
    }

    /**
     * @return The LinkedMonitors
     */
    public String getLinkedMonitors() {
        return LinkedMonitors;
    }

    /**
     * @param LinkedMonitors The LinkedMonitors
     */
    public void setLinkedMonitors(String LinkedMonitors) {
        this.LinkedMonitors = LinkedMonitors;
    }

    /**
     * @return The Triggers
     */
    public String getTriggers() {
        return Triggers;
    }

    /**
     * @param Triggers The Triggers
     */
    public void setTriggers(String Triggers) {
        this.Triggers = Triggers;
    }

    /**
     * @return The Device
     */
    public String getDevice() {
        return Device;
    }

    /**
     * @param Device The Device
     */
    public void setDevice(String Device) {
        this.Device = Device;
    }

    /**
     * @return The Channel
     */
    public String getChannel() {
        return Channel;
    }

    /**
     * @param Channel The Channel
     */
    public void setChannel(String Channel) {
        this.Channel = Channel;
    }

    /**
     * @return The Format
     */
    public String getFormat() {
        return Format;
    }

    /**
     * @param Format The Format
     */
    public void setFormat(String Format) {
        this.Format = Format;
    }

    /**
     * @return The V4LMultiBuffer
     */
    public boolean isV4LMultiBuffer() {
        return V4LMultiBuffer;
    }

    /**
     * @param V4LMultiBuffer The V4LMultiBuffer
     */
    public void setV4LMultiBuffer(boolean V4LMultiBuffer) {
        this.V4LMultiBuffer = V4LMultiBuffer;
    }

    /**
     * @return The V4LCapturesPerFrame
     */
    public String getV4LCapturesPerFrame() {
        return V4LCapturesPerFrame;
    }

    /**
     * @param V4LCapturesPerFrame The V4LCapturesPerFrame
     */
    public void setV4LCapturesPerFrame(String V4LCapturesPerFrame) {
        this.V4LCapturesPerFrame = V4LCapturesPerFrame;
    }

    /**
     * @return The Protocol
     */
    public String getProtocol() {
        return Protocol;
    }

    /**
     * @param Protocol The Protocol
     */
    public void setProtocol(String Protocol) {
        this.Protocol = Protocol;
    }

    /**
     * @return The Method
     */
    public String getMethod() {
        return Method;
    }

    /**
     * @param Method The Method
     */
    public void setMethod(String Method) {
        this.Method = Method;
    }

    /**
     * @return The Host
     */
    public String getHost() {
        return Host;
    }

    /**
     * @param Host The Host
     */
    public void setHost(String Host) {
        this.Host = Host;
    }

    /**
     * @return The Port
     */
    public String getPort() {
        return Port;
    }

    /**
     * @param Port The Port
     */
    public void setPort(String Port) {
        this.Port = Port;
    }

    /**
     * @return The SubPath
     */
    public String getSubPath() {
        return SubPath;
    }

    /**
     * @param SubPath The SubPath
     */
    public void setSubPath(String SubPath) {
        this.SubPath = SubPath;
    }

    /**
     * @return The Path
     */
    public String getPath() {
        return Path;
    }

    /**
     * @param Path The Path
     */
    public void setPath(String Path) {
        this.Path = Path;
    }

    /**
     * @return The Options
     */
    public String getOptions() {
        return Options;
    }

    /**
     * @param Options The Options
     */
    public void setOptions(String Options) {
        this.Options = Options;
    }

    /**
     * @return The User
     */
    public String getUser() {
        return User;
    }

    /**
     * @param User The User
     */
    public void setUser(String User) {
        this.User = User;
    }

    /**
     * @return The Pass
     */
    public String getPass() {
        return Pass;
    }

    /**
     * @param Pass The Pass
     */
    public void setPass(String Pass) {
        this.Pass = Pass;
    }

    /**
     * @return The Width
     */
    public String getWidth() {
        return Width;
    }

    /**
     * @param Width The Width
     */
    public void setWidth(String Width) {
        this.Width = Width;
    }

    /**
     * @return The Height
     */
    public String getHeight() {
        return Height;
    }

    /**
     * @param Height The Height
     */
    public void setHeight(String Height) {
        this.Height = Height;
    }

    /**
     * @return The Colours
     */
    public String getColours() {
        return Colours;
    }

    /**
     * @param Colours The Colours
     */
    public void setColours(String Colours) {
        this.Colours = Colours;
    }

    /**
     * @return The Palette
     */
    public String getPalette() {
        return Palette;
    }

    /**
     * @param Palette The Palette
     */
    public void setPalette(String Palette) {
        this.Palette = Palette;
    }

    /**
     * @return The Orientation
     */
    public String getOrientation() {
        return Orientation;
    }

    /**
     * @param Orientation The Orientation
     */
    public void setOrientation(String Orientation) {
        this.Orientation = Orientation;
    }

    /**
     * @return The Deinterlacing
     */
    public String getDeinterlacing() {
        return Deinterlacing;
    }

    /**
     * @param Deinterlacing The Deinterlacing
     */
    public void setDeinterlacing(String Deinterlacing) {
        this.Deinterlacing = Deinterlacing;
    }

    /**
     * @return The RTSPDescribe
     */
    public boolean isRTSPDescribe() {
        return RTSPDescribe;
    }

    /**
     * @param RTSPDescribe The RTSPDescribe
     */
    public void setRTSPDescribe(boolean RTSPDescribe) {
        this.RTSPDescribe = RTSPDescribe;
    }

    /**
     * @return The Brightness
     */
    public String getBrightness() {
        return Brightness;
    }

    /**
     * @param Brightness The Brightness
     */
    public void setBrightness(String Brightness) {
        this.Brightness = Brightness;
    }

    /**
     * @return The Contrast
     */
    public String getContrast() {
        return Contrast;
    }

    /**
     * @param Contrast The Contrast
     */
    public void setContrast(String Contrast) {
        this.Contrast = Contrast;
    }

    /**
     * @return The Hue
     */
    public String getHue() {
        return Hue;
    }

    /**
     * @param Hue The Hue
     */
    public void setHue(String Hue) {
        this.Hue = Hue;
    }

    /**
     * @return The Colour
     */
    public String getColour() {
        return Colour;
    }

    /**
     * @param Colour The Colour
     */
    public void setColour(String Colour) {
        this.Colour = Colour;
    }

    /**
     * @return The EventPrefix
     */
    public String getEventPrefix() {
        return EventPrefix;
    }

    /**
     * @param EventPrefix The EventPrefix
     */
    public void setEventPrefix(String EventPrefix) {
        this.EventPrefix = EventPrefix;
    }

    /**
     * @return The LabelFormat
     */
    public String getLabelFormat() {
        return LabelFormat;
    }

    /**
     * @param LabelFormat The LabelFormat
     */
    public void setLabelFormat(String LabelFormat) {
        this.LabelFormat = LabelFormat;
    }

    /**
     * @return The LabelX
     */
    public String getLabelX() {
        return LabelX;
    }

    /**
     * @param LabelX The LabelX
     */
    public void setLabelX(String LabelX) {
        this.LabelX = LabelX;
    }

    /**
     * @return The LabelY
     */
    public String getLabelY() {
        return LabelY;
    }

    /**
     * @param LabelY The LabelY
     */
    public void setLabelY(String LabelY) {
        this.LabelY = LabelY;
    }

    /**
     * @return The LabelSize
     */
    public String getLabelSize() {
        return LabelSize;
    }

    /**
     * @param LabelSize The LabelSize
     */
    public void setLabelSize(String LabelSize) {
        this.LabelSize = LabelSize;
    }

    /**
     * @return The ImageBufferCount
     */
    public String getImageBufferCount() {
        return ImageBufferCount;
    }

    /**
     * @param ImageBufferCount The ImageBufferCount
     */
    public void setImageBufferCount(String ImageBufferCount) {
        this.ImageBufferCount = ImageBufferCount;
    }

    /**
     * @return The WarmupCount
     */
    public String getWarmupCount() {
        return WarmupCount;
    }

    /**
     * @param WarmupCount The WarmupCount
     */
    public void setWarmupCount(String WarmupCount) {
        this.WarmupCount = WarmupCount;
    }

    /**
     * @return The PreEventCount
     */
    public String getPreEventCount() {
        return PreEventCount;
    }

    /**
     * @param PreEventCount The PreEventCount
     */
    public void setPreEventCount(String PreEventCount) {
        this.PreEventCount = PreEventCount;
    }

    /**
     * @return The PostEventCount
     */
    public String getPostEventCount() {
        return PostEventCount;
    }

    /**
     * @param PostEventCount The PostEventCount
     */
    public void setPostEventCount(String PostEventCount) {
        this.PostEventCount = PostEventCount;
    }

    /**
     * @return The StreamReplayBuffer
     */
    public String getStreamReplayBuffer() {
        return StreamReplayBuffer;
    }

    /**
     * @param StreamReplayBuffer The StreamReplayBuffer
     */
    public void setStreamReplayBuffer(String StreamReplayBuffer) {
        this.StreamReplayBuffer = StreamReplayBuffer;
    }

    /**
     * @return The AlarmFrameCount
     */
    public String getAlarmFrameCount() {
        return AlarmFrameCount;
    }

    /**
     * @param AlarmFrameCount The AlarmFrameCount
     */
    public void setAlarmFrameCount(String AlarmFrameCount) {
        this.AlarmFrameCount = AlarmFrameCount;
    }

    /**
     * @return The SectionLength
     */
    public String getSectionLength() {
        return SectionLength;
    }

    /**
     * @param SectionLength The SectionLength
     */
    public void setSectionLength(String SectionLength) {
        this.SectionLength = SectionLength;
    }

    /**
     * @return The FrameSkip
     */
    public String getFrameSkip() {
        return FrameSkip;
    }

    /**
     * @param FrameSkip The FrameSkip
     */
    public void setFrameSkip(String FrameSkip) {
        this.FrameSkip = FrameSkip;
    }

    /**
     * @return The MotionFrameSkip
     */
    public String getMotionFrameSkip() {
        return MotionFrameSkip;
    }

    /**
     * @param MotionFrameSkip The MotionFrameSkip
     */
    public void setMotionFrameSkip(String MotionFrameSkip) {
        this.MotionFrameSkip = MotionFrameSkip;
    }

    /**
     * @return The AnalysisFPS
     */
    public String getAnalysisFPS() {
        return AnalysisFPS;
    }

    /**
     * @param AnalysisFPS The AnalysisFPS
     */
    public void setAnalysisFPS(String AnalysisFPS) {
        this.AnalysisFPS = AnalysisFPS;
    }

    /**
     * @return The AnalysisUpdateDelay
     */
    public String getAnalysisUpdateDelay() {
        return AnalysisUpdateDelay;
    }

    /**
     * @param AnalysisUpdateDelay The AnalysisUpdateDelay
     */
    public void setAnalysisUpdateDelay(String AnalysisUpdateDelay) {
        this.AnalysisUpdateDelay = AnalysisUpdateDelay;
    }

    /**
     * @return The MaxFPS
     */
    public String getMaxFPS() {
        return MaxFPS;
    }

    /**
     * @param MaxFPS The MaxFPS
     */
    public void setMaxFPS(String MaxFPS) {
        this.MaxFPS = MaxFPS;
    }

    /**
     * @return The AlarmMaxFPS
     */
    public String getAlarmMaxFPS() {
        return AlarmMaxFPS;
    }

    /**
     * @param AlarmMaxFPS The AlarmMaxFPS
     */
    public void setAlarmMaxFPS(String AlarmMaxFPS) {
        this.AlarmMaxFPS = AlarmMaxFPS;
    }

    /**
     * @return The FPSReportInterval
     */
    public String getFPSReportInterval() {
        return FPSReportInterval;
    }

    /**
     * @param FPSReportInterval The FPSReportInterval
     */
    public void setFPSReportInterval(String FPSReportInterval) {
        this.FPSReportInterval = FPSReportInterval;
    }

    /**
     * @return The RefBlendPerc
     */
    public String getRefBlendPerc() {
        return RefBlendPerc;
    }

    /**
     * @param RefBlendPerc The RefBlendPerc
     */
    public void setRefBlendPerc(String RefBlendPerc) {
        this.RefBlendPerc = RefBlendPerc;
    }

    /**
     * @return The AlarmRefBlendPerc
     */
    public String getAlarmRefBlendPerc() {
        return AlarmRefBlendPerc;
    }

    /**
     * @param AlarmRefBlendPerc The AlarmRefBlendPerc
     */
    public void setAlarmRefBlendPerc(String AlarmRefBlendPerc) {
        this.AlarmRefBlendPerc = AlarmRefBlendPerc;
    }

    /**
     * @return The Controllable
     */
    public String getControllable() {
        return Controllable;
    }

    /**
     * @param Controllable The Controllable
     */
    public void setControllable(String Controllable) {
        this.Controllable = Controllable;
    }

    /**
     * @return The ControlId
     */
    public String getControlId() {
        return ControlId;
    }

    /**
     * @param ControlId The ControlId
     */
    public void setControlId(String ControlId) {
        this.ControlId = ControlId;
    }

    /**
     * @return The ControlDevice
     */
    public Object getControlDevice() {
        return ControlDevice;
    }

    /**
     * @param ControlDevice The ControlDevice
     */
    public void setControlDevice(Object ControlDevice) {
        this.ControlDevice = ControlDevice;
    }

    /**
     * @return The ControlAddress
     */
    public Object getControlAddress() {
        return ControlAddress;
    }

    /**
     * @param ControlAddress The ControlAddress
     */
    public void setControlAddress(Object ControlAddress) {
        this.ControlAddress = ControlAddress;
    }

    /**
     * @return The AutoStopTimeout
     */
    public Object getAutoStopTimeout() {
        return AutoStopTimeout;
    }

    /**
     * @param AutoStopTimeout The AutoStopTimeout
     */
    public void setAutoStopTimeout(Object AutoStopTimeout) {
        this.AutoStopTimeout = AutoStopTimeout;
    }

    /**
     * @return The TrackMotion
     */
    public String getTrackMotion() {
        return TrackMotion;
    }

    /**
     * @param TrackMotion The TrackMotion
     */
    public void setTrackMotion(String TrackMotion) {
        this.TrackMotion = TrackMotion;
    }

    /**
     * @return The TrackDelay
     */
    public String getTrackDelay() {
        return TrackDelay;
    }

    /**
     * @param TrackDelay The TrackDelay
     */
    public void setTrackDelay(String TrackDelay) {
        this.TrackDelay = TrackDelay;
    }

    /**
     * @return The ReturnLocation
     */
    public String getReturnLocation() {
        return ReturnLocation;
    }

    /**
     * @param ReturnLocation The ReturnLocation
     */
    public void setReturnLocation(String ReturnLocation) {
        this.ReturnLocation = ReturnLocation;
    }

    /**
     * @return The ReturnDelay
     */
    public String getReturnDelay() {
        return ReturnDelay;
    }

    /**
     * @param ReturnDelay The ReturnDelay
     */
    public void setReturnDelay(String ReturnDelay) {
        this.ReturnDelay = ReturnDelay;
    }

    /**
     * @return The DefaultView
     */
    public String getDefaultView() {
        return DefaultView;
    }

    /**
     * @param DefaultView The DefaultView
     */
    public void setDefaultView(String DefaultView) {
        this.DefaultView = DefaultView;
    }

    /**
     * @return The DefaultRate
     */
    public String getDefaultRate() {
        return DefaultRate;
    }

    /**
     * @param DefaultRate The DefaultRate
     */
    public void setDefaultRate(String DefaultRate) {
        this.DefaultRate = DefaultRate;
    }

    /**
     * @return The DefaultScale
     */
    public String getDefaultScale() {
        return DefaultScale;
    }

    /**
     * @param DefaultScale The DefaultScale
     */
    public void setDefaultScale(String DefaultScale) {
        this.DefaultScale = DefaultScale;
    }

    /**
     * @return The SignalCheckColour
     */
    public String getSignalCheckColour() {
        return SignalCheckColour;
    }

    /**
     * @param SignalCheckColour The SignalCheckColour
     */
    public void setSignalCheckColour(String SignalCheckColour) {
        this.SignalCheckColour = SignalCheckColour;
    }

    /**
     * @return The WebColour
     */
    public String getWebColour() {
        return WebColour;
    }

    /**
     * @param WebColour The WebColour
     */
    public void setWebColour(String WebColour) {
        this.WebColour = WebColour;
    }

    /**
     * @return The Exif
     */
    public boolean isExif() {
        return Exif;
    }

    /**
     * @param Exif The Exif
     */
    public void setExif(boolean Exif) {
        this.Exif = Exif;
    }

    /**
     * @return The Sequence
     */
    public String getSequence() {
        return Sequence;
    }

    /**
     * @param Sequence The Sequence
     */
    public void setSequence(String Sequence) {
        this.Sequence = Sequence;
    }
}
