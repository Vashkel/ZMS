
package by.vshkl.zms.model;

import java.util.ArrayList;
import java.util.List;

public class Events {

    private List<Event> events = new ArrayList<Event>();
    private Pagination pagination;

    /**
     * 
     * @return
     *     The events
     */
    public List<Event> getEvents() {
        return events;
    }

    /**
     * 
     * @param events
     *     The events
     */
    public void setEvents(List<Event> events) {
        this.events = events;
    }

    /**
     * Append events to list
     *
     * @param events
     *     The events
     */
    public void addEvents(List<Event> events) {
        this.events.addAll(events);
    }

    /**
     * 
     * @return
     *     The pagination
     */
    public Pagination getPagination() {
        return pagination;
    }

    /**
     * 
     * @param pagination
     *     The pagination
     */
    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

}
