
package by.vshkl.zms.model;

import java.util.ArrayList;
import java.util.List;

public class Options {

    private List<Object> order = new ArrayList<Object>();

    /**
     * 
     * @return
     *     The order
     */
    public List<Object> getOrder() {
        return order;
    }

    /**
     * 
     * @param order
     *     The order
     */
    public void setOrder(List<Object> order) {
        this.order = order;
    }

}
