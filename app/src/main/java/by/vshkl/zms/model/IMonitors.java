package by.vshkl.zms.model;

import java.util.List;

public interface IMonitors {

    List<Monitor> getMonitors();
}
