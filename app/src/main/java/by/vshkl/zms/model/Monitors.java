package by.vshkl.zms.model;

import java.util.ArrayList;
import java.util.List;

public class Monitors implements IMonitors{

    private List<Monitor> monitors = new ArrayList<>();

    public Monitors() {
    }

    /**
     * @return The monitors
     */
    public List<Monitor> getMonitors() {
        return monitors;
    }

    /**
     * @param monitors The monitors
     */
    public void setMonitors(List<Monitor> monitors) {
        this.monitors = monitors;
    }
}
