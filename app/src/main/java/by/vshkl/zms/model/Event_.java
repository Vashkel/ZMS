
package by.vshkl.zms.model;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Event_ {

    private String Id;
    private String MonitorId;
    private String Name;
    private String Cause;
    private String StartTime;
    private String EndTime;
    private String Width;
    private String Height;
    private String Length;
    private String Frames;
    private String AlarmFrames;
    private String TotScore;
    private String AvgScore;
    private String MaxScore;
    private String Archived;
    private String Videoed;
    private String Uploaded;
    private String Emailed;
    private String Messaged;
    private String Executed;
    private String Notes;

    /**
     * 
     * @return
     *     The Id
     */
    public String getId() {
        return Id;
    }

    /**
     * 
     * @param Id
     *     The Id
     */
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     * 
     * @return
     *     The MonitorId
     */
    public String getMonitorId() {
        return MonitorId;
    }

    /**
     * 
     * @param MonitorId
     *     The MonitorId
     */
    public void setMonitorId(String MonitorId) {
        this.MonitorId = MonitorId;
    }

    /**
     * 
     * @return
     *     The Name
     */
    public String getName() {
        return Name;
    }

    /**
     * 
     * @param Name
     *     The Name
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * 
     * @return
     *     The Cause
     */
    public String getCause() {
        return Cause;
    }

    /**
     * 
     * @param Cause
     *     The Cause
     */
    public void setCause(String Cause) {
        this.Cause = Cause;
    }

    /**
     * 
     * @return
     *     The StartTime
     */
    public String getStartTime() {
        return StartTime;
    }

    /**
     * 
     * @param StartTime
     *     The StartTime
     */
    public void setStartTime(String StartTime) {
        this.StartTime = StartTime;
    }

    /**
     * 
     * @return
     *     The EndTime
     */
    public String getEndTime() {
        return EndTime;
    }

    public int getMinutesDelta() {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm:ss");

        DateTime startTime = formatter.parseDateTime(getStartTime());
        DateTime endTime = formatter.parseDateTime(getEndTime());

        long delta = endTime.getMillis() - startTime.getMillis();

        return (int) delta / 60000;
    }

    /**
     * 
     * @param EndTime
     *     The EndTime
     */
    public void setEndTime(String EndTime) {
        this.EndTime = EndTime;
    }

    /**
     * 
     * @return
     *     The Width
     */
    public String getWidth() {
        return Width;
    }

    /**
     * 
     * @param Width
     *     The Width
     */
    public void setWidth(String Width) {
        this.Width = Width;
    }

    /**
     * 
     * @return
     *     The Height
     */
    public String getHeight() {
        return Height;
    }

    /**
     * 
     * @param Height
     *     The Height
     */
    public void setHeight(String Height) {
        this.Height = Height;
    }

    /**
     * 
     * @return
     *     The Length
     */
    public String getLength() {
        return Length;
    }

    /**
     * 
     * @param Length
     *     The Length
     */
    public void setLength(String Length) {
        this.Length = Length;
    }

    /**
     * 
     * @return
     *     The Frames
     */
    public String getFrames() {
        return Frames;
    }

    /**
     * 
     * @param Frames
     *     The Frames
     */
    public void setFrames(String Frames) {
        this.Frames = Frames;
    }

    /**
     * 
     * @return
     *     The AlarmFrames
     */
    public String getAlarmFrames() {
        return AlarmFrames;
    }

    /**
     * 
     * @param AlarmFrames
     *     The AlarmFrames
     */
    public void setAlarmFrames(String AlarmFrames) {
        this.AlarmFrames = AlarmFrames;
    }

    /**
     * 
     * @return
     *     The TotScore
     */
    public String getTotScore() {
        return TotScore;
    }

    /**
     * 
     * @param TotScore
     *     The TotScore
     */
    public void setTotScore(String TotScore) {
        this.TotScore = TotScore;
    }

    /**
     * 
     * @return
     *     The AvgScore
     */
    public String getAvgScore() {
        return AvgScore;
    }

    /**
     * 
     * @param AvgScore
     *     The AvgScore
     */
    public void setAvgScore(String AvgScore) {
        this.AvgScore = AvgScore;
    }

    /**
     * 
     * @return
     *     The MaxScore
     */
    public String getMaxScore() {
        return MaxScore;
    }

    /**
     * 
     * @param MaxScore
     *     The MaxScore
     */
    public void setMaxScore(String MaxScore) {
        this.MaxScore = MaxScore;
    }

    /**
     * 
     * @return
     *     The Archived
     */
    public String getArchived() {
        return Archived;
    }

    /**
     * 
     * @param Archived
     *     The Archived
     */
    public void setArchived(String Archived) {
        this.Archived = Archived;
    }

    /**
     * 
     * @return
     *     The Videoed
     */
    public String getVideoed() {
        return Videoed;
    }

    /**
     * 
     * @param Videoed
     *     The Videoed
     */
    public void setVideoed(String Videoed) {
        this.Videoed = Videoed;
    }

    /**
     * 
     * @return
     *     The Uploaded
     */
    public String getUploaded() {
        return Uploaded;
    }

    /**
     * 
     * @param Uploaded
     *     The Uploaded
     */
    public void setUploaded(String Uploaded) {
        this.Uploaded = Uploaded;
    }

    /**
     * 
     * @return
     *     The Emailed
     */
    public String getEmailed() {
        return Emailed;
    }

    /**
     * 
     * @param Emailed
     *     The Emailed
     */
    public void setEmailed(String Emailed) {
        this.Emailed = Emailed;
    }

    /**
     * 
     * @return
     *     The Messaged
     */
    public String getMessaged() {
        return Messaged;
    }

    /**
     * 
     * @param Messaged
     *     The Messaged
     */
    public void setMessaged(String Messaged) {
        this.Messaged = Messaged;
    }

    /**
     * 
     * @return
     *     The Executed
     */
    public String getExecuted() {
        return Executed;
    }

    /**
     * 
     * @param Executed
     *     The Executed
     */
    public void setExecuted(String Executed) {
        this.Executed = Executed;
    }

    /**
     * 
     * @return
     *     The Notes
     */
    public String getNotes() {
        return Notes;
    }

    /**
     * 
     * @param Notes
     *     The Notes
     */
    public void setNotes(String Notes) {
        this.Notes = Notes;
    }

}
