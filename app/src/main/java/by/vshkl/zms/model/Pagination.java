
package by.vshkl.zms.model;

import java.util.ArrayList;
import java.util.List;

public class Pagination {

    private int page;
    private int current;
    private int count;
    private boolean prevPage;
    private boolean nextPage;
    private int pageCount;
    private List<Object> order = new ArrayList<>();
    private int limit;
    private Options options;
    private String paramType;

    /**
     * 
     * @return
     *     The page
     */
    public int getPage() {
        return page;
    }

    /**
     * 
     * @param page
     *     The page
     */
    public void setPage(int page) {
        this.page = page;
    }

    /**
     * 
     * @return
     *     The current
     */
    public int getCurrent() {
        return current;
    }

    /**
     * 
     * @param current
     *     The current
     */
    public void setCurrent(int current) {
        this.current = current;
    }

    /**
     * 
     * @return
     *     The count
     */
    public int getCount() {
        return count;
    }

    /**
     * 
     * @param count
     *     The count
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * 
     * @return
     *     The prevPage
     */
    public boolean isPrevPage() {
        return prevPage;
    }

    /**
     * 
     * @param prevPage
     *     The prevPage
     */
    public void setPrevPage(boolean prevPage) {
        this.prevPage = prevPage;
    }

    /**
     * 
     * @return
     *     The nextPage
     */
    public boolean isNextPage() {
        return nextPage;
    }

    /**
     * 
     * @param nextPage
     *     The nextPage
     */
    public void setNextPage(boolean nextPage) {
        this.nextPage = nextPage;
    }

    /**
     * 
     * @return
     *     The pageCount
     */
    public int getPageCount() {
        return pageCount;
    }

    /**
     * 
     * @param pageCount
     *     The pageCount
     */
    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    /**
     * 
     * @return
     *     The order
     */
    public List<Object> getOrder() {
        return order;
    }

    /**
     * 
     * @param order
     *     The order
     */
    public void setOrder(List<Object> order) {
        this.order = order;
    }

    /**
     * 
     * @return
     *     The limit
     */
    public int getLimit() {
        return limit;
    }

    /**
     * 
     * @param limit
     *     The limit
     */
    public void setLimit(int limit) {
        this.limit = limit;
    }

    /**
     * 
     * @return
     *     The options
     */
    public Options getOptions() {
        return options;
    }

    /**
     * 
     * @param options
     *     The options
     */
    public void setOptions(Options options) {
        this.options = options;
    }

    /**
     * 
     * @return
     *     The paramType
     */
    public String getParamType() {
        return paramType;
    }

    /**
     * 
     * @param paramType
     *     The paramType
     */
    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

}
