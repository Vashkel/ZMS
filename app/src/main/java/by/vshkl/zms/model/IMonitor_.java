package by.vshkl.zms.model;

public interface IMonitor_ {

    String getId();

    String getName();

    String getServerId();

    String getType();

    String getFunction();

    String getEnabled();

    String getLinkedMonitors();

    String getTriggers();

    String getDevice();

    String getChannel();

    String getFormat();

    boolean isV4LMultiBuffer();

    String getV4LCapturesPerFrame();

    String getProtocol();

    String getMethod();

    String getHost();

    String getPort();

    String getSubPath();

    String getPath();

    String getOptions();

    String getUser();

    String getPass();

    String getWidth();

    String getHeight();

    String getColours();

    String getPalette();

    String getOrientation();

    String getDeinterlacing();

    boolean isRTSPDescribe();

    String getBrightness();

    String getContrast();

    String getHue();

    String getColour();

    String getEventPrefix();

    String getLabelFormat();

    String getLabelX();

    String getLabelY();

    String getLabelSize();

    String getImageBufferCount();

    String getWarmupCount();

    String getPreEventCount();

    String getPostEventCount();

    String getStreamReplayBuffer();

    String getAlarmFrameCount();

    String getSectionLength();

    String getFrameSkip();

    String getMotionFrameSkip();

    String getAnalysisFPS();

    String getAnalysisUpdateDelay();

    String getMaxFPS();

    String getAlarmMaxFPS();

    String getFPSReportInterval();

    String getRefBlendPerc();

    String getAlarmRefBlendPerc();

    String getControllable();

    String getControlId();

    Object getControlDevice();

    Object getControlAddress();

    Object getAutoStopTimeout();

    String getTrackMotion();

    String getTrackDelay();

    String getReturnLocation();

    String getReturnDelay();

    String getDefaultView();

    String getDefaultRate();

    String getDefaultScale();

    String getSignalCheckColour();

    String getWebColour();

    boolean isExif();

    String getSequence();
}
