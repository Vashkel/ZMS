package by.vshkl.zms.model;

public class Monitor implements IMonitor{

    private Monitor_ Monitor;

    public Monitor() {
    }

    /**
     * @return The Monitor
     */
    public Monitor_ getMonitor() {
        return Monitor;
    }

    /**
     * @param Monitor The Monitor
     */
    public void setMonitor(Monitor_ Monitor) {
        this.Monitor = Monitor;
    }
}
