
package by.vshkl.zms.model;

public class Event {

    private Event_ Event;
    private String thumbData;

    /**
     * 
     * @return
     *     The Event
     */
    public Event_ getEvent() {
        return Event;
    }

    /**
     * 
     * @param Event
     *     The Event
     */
    public void setEvent(Event_ Event) {
        this.Event = Event;
    }

    /**
     * 
     * @return
     *     The thumbData
     */
    public String getThumbData() {
        return thumbData;
    }

    /**
     * 
     * @param thumbData
     *     The thumbData
     */
    public void setThumbData(String thumbData) {
        this.thumbData = thumbData;
    }

}
